<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link as="font" crossorigin="anonymous" href="/wp-content/plugins/wp-plugin-lg-plugin/assets/lg-slider/fonts/slick.woff" rel="preload">


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header">
    <div class="header-content">
      <?php do_action( "lg_announcement_bar") ?>
      <?php get_template_part("/templates/template-parts/site-utility-bar"); ?>

      <div class="header-main">
    		<div class="site-branding">
    			<div class="logo">
    				<?php
    					if(shortcode_exists('lg-site-logo')){
    						echo do_shortcode('[lg-site-logo]');
    					}else{
    						the_custom_logo();
    					}
    			 	?>
    			</div>

          <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
    		</div><!-- .site-branding -->

        <div class="main-navigation">
          	<nav class="navbar navbar-default">  
          	    <!-- Brand and toggle get grouped for better mobile display -->
          	    <div class="navbar-header">
          	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
          	        <span class="sr-only">Toggle navigation</span>
          	        <span class="icon-bar"></span>
          	        <span class="icon-bar"></span>
          	        <span class="icon-bar"></span>
          	      </button>
          	    </div>


                <!-- Main Menu  -->
                <?php 

                  $mainMenu = array(
                  	'menu'              => 'main-menu',
                  	// 'theme_location'    => 'top-nav',
                  	'depth'             => 2,
                  	'container'         => 'div',
                  	'container_class'   => 'navbar-collapser',
                  	'container_id'      => 'main-navbar',
                  	'menu_class'        => 'nav navbar-nav',
                  	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                  		// 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
                  	'walker'            => new WP_Bootstrap_Navwalker()
                  );
                  wp_nav_menu($mainMenu);

                ?>
          	</nav>
        </div>
      </div>
    </div>

  </header><!-- #masthead -->
