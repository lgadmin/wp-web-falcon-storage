<?php

function lg_enqueue_styles_scripts() {
    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], '1.1' );
	
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:300,300i,400,400i,700,700i&display=swap', false );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), '1.0.1' );
	wp_register_script( 'vendorJS', get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js", array('jquery'), false );


	wp_enqueue_script( 'lg-script' );
	wp_enqueue_script( 'vendorJS' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function lg_dequeue_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );

}
//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );


// dequeue stylesheets and scripts conditionally
function lg_conditional_scripts_styles() {
	wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'lg_conditional_scripts_styles', 100);

/*dequeue wp-embed.js*/
function lg_deregister_embed(){
	wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'lg_deregister_embed' );
