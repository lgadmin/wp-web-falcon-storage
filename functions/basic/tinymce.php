<?php
function add_style_select_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

		$style_formats = array(
			/*
			* Each array child is a format with it's own settings
			* Notice that each array has title, block, classes, and wrapper arguments
			* Title is the label which will be visible in Formats menu
			* Block defines whether it is a span, div, selector, or inline style
			* Classes allows you to define CSS classes
			* Wrapper whether or not to add a new block-level element around any selected elements
			*/
            array(
                'title'   => 'Primary Text',
                'selector'   => 'h1, h2, h3, h4, h5, h6, p',
                'classes' => 'primary-text',
            ),
			array(
				'title'   => 'Secondary Text',
				'selector'   => 'h1, h2, h3, h4, h5, h6, p',
				'classes' => 'secondary-text',
			),
			array(
				'title'   => 'Font Weight Bold',
				'selector'   => 'h1, h2, h3, h4, h5, h6, p',
				'classes' => 'text-500',
			),
		);
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );

		return $init_array;

}
	// Attach callback to 'tiny_mce_before_init'
	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
