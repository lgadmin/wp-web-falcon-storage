<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<?php 
		$footer_background_image = get_field('footer_background_image', 'option');
	?>

	<footer id="colophon" class="site-footer">
		<div id="site-footer" class="h-padding clearfix pb-md pt-md" style="background-image: url('<?php echo $footer_background_image; ?>');">
			<div class="footer-logo">
				<?php
    				if(shortcode_exists('lg-site-logo')){
						echo do_shortcode('[lg-site-logo]');
					}else{
						the_custom_logo();
					}

					$footer_blurb = get_field('footer_blurb', 'option');
					echo $footer_blurb;
    			 ?>
			</div>
			<div class="nav-footer"><?php get_template_part("/templates/template-parts/nav-footer"); ?></div>
			<div class="footer-utility">
				<?php get_template_part("/templates/template-parts/address-card"); ?>
				<!-- <a target="_blank" class='tenant-login-link' href="https://www.smdservers.net/SLWebSiteTemplate/login.aspx?sCorpCode=zCdibPjc3p424v3X9Yv0rw==&sLocationCode=F3e81MY3f4IcaovtqM7F3w==">Tenant Login</a> -->
				<?php if(have_rows('footer_social_media', 'option')) : ?>
					<div class='footer-social-media'>
					<?php while(have_rows('footer_social_media', 'option' )) : the_row(); ?>
						<?php
							$image = get_sub_field('image', 'option');
							$link = get_sub_field('link', 'option');
							$size='full'
						?>
						
							<a class='mr-2' target="_blank" rel="noopener" href="<?php echo $link; ?>"><?php echo wp_get_attachment_image( $image['id'], $size ); ?></a>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div id="site-legal" class="pb-xs h-padding pt-xs clearfix">
			<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
