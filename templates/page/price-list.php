<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content faq">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part( '/templates/template-parts/site-top-banner' ); ?>
				<!-- end Top Banner -->

				<!-- Price List -->
				<?php
					$page_title                = get_field( 'page_title' );
					$page_sub_title            = get_field( 'page_sub_title' );
					$first_floor_inside_title  = get_field( 'first_floor_inside_title' );
					$second_floor_inside_title = get_field( 'second_floor_inside_title' );
					$driveway_access_title     = get_field( 'driveway_access_title' );
				?>
				<div class="price-list-page">
					<div class="container ng-mt pb-lg h-padding">
					<div class="price-list">
						<div class="price-list-title text-center">
							<h2 class="h1"><?php echo $page_title; ?></h2>
							<p><?php echo $page_sub_title; ?></p>
						</div>
						<div class="first-floor-inside storage-type">
							<h2><?php echo $first_floor_inside_title; ?></h2>
							<div class="price-list-table">
							<?php if ( have_rows( 'first_floor_inside_specs' ) ) : ?>
								<div class="price-list-header-table">
									 <h4 class="h6">UNIT</h4>
									 <h4 class="h6">PRICE</h4>
								</div>
								<?php
								while ( have_rows( 'first_floor_inside_specs' ) ) :
									the_row();
									?>
									<?php
									$unit  = get_sub_field( 'unit' );
									$price = get_sub_field( 'price' );
									?>
									<div class="price-list-row">
										<div><?php echo $unit; ?></div>
										<div><?php echo $price; ?></div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
							</div>
						</div>
						<!-- End of first floor inside -->
						<div class="second-floor-inside storage-type">
							<h2><?php echo $second_floor_inside_title; ?></h2>
							<div class="price-list-table">
							<?php if ( have_rows( 'second_floor_inside_specs' ) ) : ?>
								<div class="price-list-header-table">
									 <h4 class="h6">UNIT</h4>
									 <h4 class="h6">PRICE</h4>
								</div>
								<?php
								while ( have_rows( 'second_floor_inside_specs' ) ) :
									the_row();
									?>
									<?php
									$unit  = get_sub_field( 'unit' );
									$price = get_sub_field( 'price' );
									?>
									<div class="price-list-row">
										<div><?php echo $unit; ?></div>
										<div><?php echo $price; ?></div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
							</div>
						</div>
						<!-- End of second floor inside -->
						<div class="driveway_access storage-type">
							<h2><?php echo $driveway_access_title; ?></h2>
							<div class="price-list-table">
							<?php if ( have_rows( 'driveway_access_specs' ) ) : ?>
								<div class="price-list-header-table">
									 <h4 class="h6">UNIT</h4>
									 <h4 class="h6">PRICE</h4>
								</div>
								<?php
								while ( have_rows( 'driveway_access_specs' ) ) :
									the_row();
									?>
									<?php
									$unit  = get_sub_field( 'unit' );
									$price = get_sub_field( 'price' );
									?>
									<div class="price-list-row">
										<div><?php echo $unit; ?></div>
										<div><?php echo $price; ?></div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
							</div>
						</div>
						<!-- End of Driveway access -->
					</div>
					</div>
				</div>

				<!-- end Price List -->

				<!-- Reservation CTA -->
				<?php get_template_part( '/templates/template-parts/reservation-cta' ); ?>
				<!-- end Reservation CTA -->
				
			</main>
		</div>
	</div>

<?php get_footer(); ?>
