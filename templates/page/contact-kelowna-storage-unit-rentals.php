<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content contact-us">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->

				<!-- Contact us -->
				<?php
					$text = get_field('text');
					$image = get_field('image');
					$driving_instructions = get_field('driving_instructions');
				?>	
				<div class="contact">
					<div class="container ng-mt">
						<h2 class="h2 container center pb-sm">Contact Us</h2>
						<div>
							<div class="split-image h-padding">
								<?php echo $text; ?>
								<p class="pt-xs pb-sm">
									<?php echo do_shortcode('[lg-address1]'); ?>, <?php echo do_shortcode('[lg-city]'); ?> <?php echo do_shortcode('[lg-province]'); ?><br>
									Phone: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a><br>
									Fax: <a href="tel:+1<?php echo do_shortcode('[lg-fax]'); ?>"><?php echo format_phone(do_shortcode('[lg-fax]')); ?></a><br>
								</p>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
								<div class="driving-instructions">
									<?php echo $driving_instructions; ?>
								</div>
							</div>
							<div class="split-copy">
								<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
							</div>
						</div>
					</div>
				</div>
				<!-- end Contact us -->

				<!-- Google Map -->
				<div class="map">
					<?php echo do_shortcode('[lg-map id=35]'); ?>
				</div>
				<!-- end Google Map -->

				<!-- Reservation CTA -->
				<?php get_template_part("/templates/template-parts/reservation-cta"); ?>
				<!-- end Reservation CTA -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>