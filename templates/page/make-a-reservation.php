<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content reservation">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->

				<!-- Make a reservation -->
				<div class="reservation-form">
					<div class="container pt-lg pb-lg">
						<h2 class="center">Reservation Form</h2>
						<div class="form">
							<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
						</div>
					</div>
				</div>
				<!-- end Make a reservation -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>