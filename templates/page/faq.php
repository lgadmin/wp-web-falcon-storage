<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content faq">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->

				<!-- FAQ -->
				<?php
				$faqs = get_field('faqs');
				?>
				<div class="faq">
					<div class="container ng-mt pb-lg h-padding">
						<h2 class="h2 container center pb-sm">Frequently asked Questions</h2>
						<?php echo do_shortcode($faqs); ?>
					</div>
				</div>
				<!-- end FAQ -->

				<!-- Reservation CTA -->
				<?php get_template_part("/templates/template-parts/reservation-cta"); ?>
				<!-- end Reservation CTA -->
				
			</main>
		</div>
	</div>

<?php get_footer(); ?>