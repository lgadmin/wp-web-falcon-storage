<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content home">
			<main>

				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->

				<!-- spring Special -->
				<?php /*
					$spring_special = get_field("spring_special");
				*/?><!--
				<?php /*if ($spring_special): */?>
					<div class="spring-special"><?php /*echo $spring_special; */?></div>
					<div class="spring-special-mobile"><?php /*echo $spring_special; */?></div>
				--><?php /*endif; */?>

				<!-- Intro Section -->
				<?php
					$intro_icon = get_field('intro_icon');
					$intro_title = get_field('intro_title');
					$intro_description = get_field('intro_description');
				?>
				<div class="intro center">
					<div class="ng-mt ng-mb h-padding container pt-lg pb-lg bg-white">
						<?php echo $intro_icon; ?>
						<?php if($intro_title): ?>
							<h1 class="h2"><?php echo $intro_title; ?></h1>
						<?php endif; ?>
						<?php echo $intro_description; ?>
					</div>
				</div>
				<!-- end Intro Section -->

				<!-- Service Section -->
				<?php
					$services_title = get_field('services_title');
					$services_description = get_field('services_description');
					$services_image = get_field('services_image');
				?>
				<div class="services pt-md bg-gray-lighter">
					<div class="container">
						<div class="split-image">
							<img src="<?php echo $services_image['url']; ?>" alt="<?php echo $services_image['alt']; ?>">
						</div>
						<div class="split-copy">
							<?php if($services_title): ?>
								<h2 class="h2"><?php echo $services_title; ?></h2>
							<?php endif; ?>
							<?php echo $services_description; ?>
						</div>
					</div>
				</div>
				<!-- end Service Section -->

				<!-- Advantages -->
				<?php
					$advantage_title = get_field('advantage_title');
				?>
				<div class="pt-lg pb-lg advantage center container">
					<?php if($advantage_title): ?>
						<h2 class="h2"><?php echo $advantage_title; ?></h2>
					<?php endif; ?>

					<?php
					if( have_rows('advantages') ):
						?>
						<div class="advantage-list pt-md">
						<?php
					    while ( have_rows('advantages') ) : the_row();
					        $icon = get_sub_field('icon');
					        $title = get_sub_field('title');
					        $content = get_sub_field('content');
					        ?>
							<div>
								<div class="thumb">
									<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
								</div>
								<?php if($title): ?>
									<h3 class="h3"><?php echo $title; ?></h3>
								<?php endif; ?>
								<?php echo $content; ?>
							</div>
					        <?php
					    endwhile;
					    ?>
						</div>
					    <?php
					else :
					    // no rows found
					endif;
					?>
				</div>
				<!-- end Advantages -->

				<!-- Reservation CTA -->
				<?php get_template_part("/templates/template-parts/reservation-cta"); ?>
				<!-- end Reservation CTA -->

				<!-- U-Haul -->
				<?php
					// $u_haul_image = get_field('u-haul_image');
					// $u_haul_title = get_field('u-haul_title');
					// $u_haul_description = get_field('u-haul_description');
					// $u_haul_link = get_field('u-haul_link');
				?>
				<!-- <div class="u-haul">
					<div class="container">
						<div class="split-image">
							<img src="<?php echo $u_haul_image['url']; ?>" alt="<?php echo $u_haul_image['alt']; ?>">
						</div>
						<div class="split-copy">
							<?php if($u_haul_title): ?>
								<h2 class="h3"><?php echo $u_haul_title; ?></h2>
							<?php endif; ?>
							<?php echo $u_haul_description; ?>
							<a href="<?php echo $u_haul_link->guid; ?>" class="learn-more">Read More</a>
						</div>
					</div>
				</div> -->
				<!-- end U-Haul -->

				<!-- Contact Us -->
				<div class="contact-us bg-gray-lighter">
					<div class="">
						<div class="split-image">
							<?php echo do_shortcode('[lg-map id=35]'); ?>
						</div>
						<div class="split-copy">
							<div>
								<h3>Contact us</h3>
								<p>
									<?php echo do_shortcode('[lg-address1]'); ?>, <?php echo do_shortcode('[lg-city]'); ?> <?php echo do_shortcode('[lg-province]'); ?><br>
									Phone: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a><br>
									Fax: <a href="tel:+1<?php echo do_shortcode('[lg-fax]'); ?>"><?php echo format_phone(do_shortcode('[lg-fax]')); ?></a><br>

								</p>
							</div>
						</div>
					</div>
				</div>
				<!-- end Contact Us -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>
