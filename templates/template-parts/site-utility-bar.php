<div class="utility-bar">
  <div class="">
    <div class="left">
      <div>
        
      </div>
    </div>
    <div class="right">
      <div class="header-address">
        <span><?php echo do_shortcode('[lg-address1]'); ?> <?php echo do_shortcode('[lg-address2]'); ?>, <?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?></span>
      </div>
      <div>
        <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></span></a>
      </div>
    </div>
  </div>
</div>