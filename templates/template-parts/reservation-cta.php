<?php
	$reservation_cta_icon = get_field('reservation_cta_icon', 'option');
	$reservation_cta_text = get_field('reservation_cta_text', 'option');
	$reservation_cta_button = get_field('reservation_cta_button', 'option');
?>

<div class="reservation-cta block pt-md pb-md">
	<div class="container">
		<div class="left">
			<img src="<?php echo $reservation_cta_icon['url']; ?>" alt="<?php echo $reservation_cta_icon['alt']; ?>">
			<h2 class="h2"><?php echo $reservation_cta_text; ?></h2>
		</div>
		<div class="right">
			<a href="<?php echo get_permalink($reservation_cta_button['button_link']->ID); ?>" class="cta-primary"><?php echo $reservation_cta_button['button_text']; ?></a>
		</div>
	</div>
</div>