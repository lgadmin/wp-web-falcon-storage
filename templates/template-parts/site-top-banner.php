<?php
	$top_banner_background_image = get_field( 'top_banner_background_image', 'option' );
?>

<div class="site-top-banner" 
<?php
if ( ! is_front_page() ) :
	?>
	style="background-image: url('<?php echo $top_banner_background_image; ?>');" <?php endif; ?>>
	<div class="title center 
	<?php
	if ( ! is_front_page() ) :
		?>
		pt-md pb-xl<?php endif; ?>">
		<?php
		if ( is_front_page() ) :
			?>
		
			<?php
			if ( have_rows( 'slider_images' ) ) :
				?>
				<div class="home-slider">
				<?php
				while ( have_rows( 'slider_images' ) ) :
					the_row();
					$image                = get_sub_field( 'image' );
					$mobile_image         = get_sub_field( 'mobile_image' );
					$top_banner_title     = get_sub_field( 'top_banner_title' );
					$top_banner_sub_title = get_sub_field( 'top_banner_sub_title' );
					$sub_title_copy       = get_sub_field( 'sub_title_copy' );
					?>
						<div>
							<picture>
							<link rel="preload" as="image" href="<?php echo $image['url']; ?>" />
								<?php if ( $mobile_image ) : ?>
									
								<source 
									media="(max-width: 480px)" 
									srcset="<?php echo wp_get_attachment_image_src( $mobile_image['ID'], 'mobile-banner' )[0]; ?>"
								>
								<?php endif; ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
							</picture>
							
							<div class="overlay">
								<?php if ( $top_banner_title ) : ?>
									<h2 class="h1"><?php echo $top_banner_title; ?></h2>
								<?php endif; ?>
								<?php if ( $top_banner_sub_title ) : ?>
									<div class="page-title"><?php echo $top_banner_sub_title; ?></div>
								<?php endif; ?>
								<?php if ( $sub_title_copy ) : ?>
									<div class="page-sub-title"><?php echo $sub_title_copy; ?></div>
								<?php endif; ?>

								<?php
								if ( have_rows( 'ctas' ) ) :
									?>
									<div class="ctas">
									<?php
									while ( have_rows( 'ctas' ) ) :
										the_row();
										$text      = get_sub_field( 'text' );
										$link_post = get_sub_field( 'link' );
										?>
										<a href="<?php echo get_permalink( $link_post->ID ); ?>" class="cta-secondary"><?php echo $text; ?></a>
										<?php
									endwhile;
									?>
									</div>
									<?php
								else :
									// no rows found
								endif;
								?>
							</div>
						</div>
					<?php
				endwhile;
				?>
				</div>
				<?php
			else :
				// no rows found
			endif;
			?>
				
			</div>
			<div class="home-shredding">
				<a href="/service/document-shredding-services/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/dist/images/home-shredding-star.png" alt="Kelowna Secure Shredding"></a>
			</div>

		<?php else : ?>
		<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php
			if ( function_exists( 'yoast_breadcrumb' ) && ! is_front_page() ) {
				yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
			}
			?>
		<?php endif; ?>
	</div>
</div>
