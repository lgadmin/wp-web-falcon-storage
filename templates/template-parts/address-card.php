<h3 class="nav-title">Contact us</h3>
<!-- Default Address Stuff -->
<div class="address-card">
	<address>

		<span class="card-map-marker">
			<span><?php echo do_shortcode('[lg-address1]'); ?></span> <span><?php echo do_shortcode('[lg-city]'); ?></span>, <span><?php echo do_shortcode('[lg-province]'); ?></span>&nbsp;<span><?php echo do_shortcode('[lg-postcode]'); ?></span><br>
		</span>
		<?php $phone = get_field('company_phone', 'option'); ?>
		<span class="card-map-phone">Tel: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>
		<span class="card-map-phone">Fax: <a href="tel:+1<?php echo do_shortcode('[lg-fax]'); ?>"><?php echo format_phone(do_shortcode('[lg-fax]')); ?></a></span><br>


	</address>

</div>

