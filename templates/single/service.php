<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content services">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->

				<!-- Services -->
				<div class="services">
					<div class="container ng-mt pb-lg h-padding">
						<h2 class="h2 container center"><?php echo get_the_title(); ?></h2>
						
						<?php 
							$intro = get_field('intro');
							if($intro){
								echo '<div class="service-intro pb-md">';
								echo $intro;
								echo '</div>';
							}
						?>
						
						<?php
						if( have_rows('sections') ):
							$count = 0;
							$cta_text = get_field('cta_text');
							$cta_link = get_field('cta_link');
							?>
							<div class="service-list">
							<?php
						    while ( have_rows('sections') ) : the_row();
						        $image = get_sub_field('image');
						        $title = get_sub_field('title');
						        $description = get_sub_field('description');
								$benefits = get_sub_field('benefits');
								$cta = get_sub_field('cta');
						        $count++;
						        ?>
								<section class="center">
									<?php if($image) : ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
									<?php endif; ?>
									<?php if($title): ?>
										<h2 class="h2"><?php echo $title; ?></h2>
									<?php endif; ?>
									<?php echo $description; ?>
									<?php 
										if($cta) : 	
										$cta_url = $cta['url'];
    									$cta_title = $cta['title'];
    									$cta_target = $cta['target'] ? $cta['target'] : '_self';
									?>
									<div class="center pt-xs">
										<a class="cta-primary" href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo esc_attr( $cta_target ); ?>"><?php echo esc_html( $cta_title ); ?></a>	
									</div>
									<?php endif; ?>
									
									<?php if (is_array($benefits)): ?>
									<?php if(sizeof($benefits) > 1): ?>
										<div class="benefits">
											<h3 class="h3">Features/Benefits</h2>
											<ul>
											<?php foreach ($benefits as $key => $benefit): ?>
												<li><?php echo $benefit['benefit']; ?></li>
											<?php endforeach; ?>
											</ul>
										</div>
									<?php endif; ?>
									<?php endif; ?>
								</section>
						        <?php
						    endwhile;
						    ?>
							</div>
						    <?php
						else :
						    // no rows found
						endif;
						?>
					</div>
				</div>
				<!-- end Services -->

				<!-- Reservation CTA -->
				<?php get_template_part("/templates/template-parts/reservation-cta"); ?>
				<!-- end Reservation CTA -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>