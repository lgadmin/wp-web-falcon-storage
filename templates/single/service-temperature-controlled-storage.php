<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content services">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->
                <?php
                $temperature_controlled_storage = get_field("temperature_controlled_storage");
                $temperature_controlled_storage_image = get_field("temperature_controlled_storage_image");
                $advanced_temperature_storage = get_field("advanced_temperature_storage");
                $extra_protection = get_field("extra_protection");
                $about_the_temperature = get_field("about_the_temperature");
                ?>
				<!-- Services -->
                <div class="services service-temperature-controlled-storage">
                    <div class="container ng-mt h-padding temperature-controlled-storage">
                        <div class="pb-md">
                            <?php the_field("temperature_controlled_storage");  ?>
                        </div>
                        <?php echo wp_get_attachment_image($temperature_controlled_storage_image["ID"], "full"); ?>
                    </div>
                    <div class="advanced-temperature-controlled-storage pb-md container">
                        <?php the_field("advanced_temperature_storage"); ?>
                    </div>
                    <div class="extra-protection container pb-md">
                        <?php the_field("extra_protection"); ?>
                        <?php if( have_rows( "extra_protection_grid" ) ) : ?>
                            <div class="extra-protection-grid">
                            <?php while( have_rows( "extra_protection_grid" ) ) : the_row(); ?>
                                <?php
                                $image = get_sub_field("image");
                                $title = get_sub_field("title");
                                ?>
                                <div>
                                    <?php echo wp_get_attachment_image($image["ID"], 'full'); ?>
                                    <?php echo $title; ?>
                                </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="about-temperature pb-md container">
                        <?php echo $about_the_temperature; ?>
                    </div>
                    <?php if( have_rows( "q_and_a" ) ) : ?>
                        
                    <div class="service-q-a">
                        <div class="container">
                        <div class="q-and-a-intro">
                            <?php the_field('q_and_a_intro'); ?>
                        </div>
                        <?php while( have_rows( "q_and_a" ) ) : the_row(); ?>
                        <?php 
                        $question = get_sub_field("question");
                        $answer = get_sub_field("answer");
                        ?>
                        <div class="q-a-group">
                            <div class="service-question">
                                <h3 class="square-bg">Q</h3>
                                <div>
                                <?php echo $question; ?>
                                </div>
                            </div>
                            <div class="service-answer">
                                <h3 class="square-bg">A</h3>
                                <div>
                                <?php echo $answer;?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        </div>
                    </div>
                    
                    <?php endif; ?>
                </div>
				<!-- end Services -->

				<!-- Reservation CTA -->
				<?php get_template_part("/templates/template-parts/reservation-cta"); ?>
				<!-- end Reservation CTA -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>