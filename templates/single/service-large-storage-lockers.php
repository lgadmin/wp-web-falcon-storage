<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content services">
			<main>
				
				<!-- Top Banner -->
				<?php get_template_part("/templates/template-parts/site-top-banner"); ?>
				<!-- end Top Banner -->

				<!-- Services -->
                <?php 
                    $locker_intro = get_field('lockers_intro');
                    $our_lockers_description = get_field('our_lockers_description');
                    $storage_uses_image = get_field('storage_uses_image');
                    $storage_uses_description = get_field('storage_uses_description');
                    $storage_benefits_title = get_field("storage_benefits_title");
                    $storage_feature_title = get_field("storage_feature_title");
                ?>
                <div class="services service-large-storage">
                    <div class="page-wrapper">
                        <div class="container ng-mt h-padding service-large-storage__intro">
                            <?php echo $locker_intro; ?>
                        </div>
                        <div class="container service-large-storage__our-lockers">
                            <div><?php echo $our_lockers_description; ?></div>
                            <div class="service-large-storage__our-lockers__formats">
                                <?php if( have_rows( "our_lockers_formats" ) ) : ?>
                                    <?php while( have_rows( "our_lockers_formats" ) ) : the_row(); ?>
                                    <div class="service-large-storage__our-lockers__content">
                                        <div>
                                        <?php echo get_sub_field( "format" ); ?>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="service-large-storage__storage-uses">
                            <div class="container">
                                <div>
                                    <?php echo wp_get_attachment_image($storage_uses_image["ID"], "full"); ?>
                                </div>
                                <div>
                                    <?php echo $storage_uses_description; ?>
                                </div>
                            </div>
                        </div>
                        <div class="service-large-storage__benefits container">
                            <?php echo $storage_benefits_title; ?>
                            <div class="service-large-storage__benefits__content">
                                <?php if( have_rows( "storage_benefits" ) ) : ?>
                                    <?php while( have_rows( "storage_benefits" ) ) : the_row(); ?>
                                    <?php 
                                        $image = get_sub_field( 'image' );
                                        $content = get_sub_field( 'content' );
                                    ?>
                                    <div>
                                        <?php echo wp_get_attachment_image($image["ID"], "full"); ?>
                                        <br />
                                        <div><?php echo $content; ?></div>
                                    </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="service-large-storage__feature container">
                            <?php echo $storage_feature_title; ?>
                            <div class="service-large-storage__feature__content">
                                <?php if( have_rows( "storage_feature" ) ) : ?>
                                    <?php while( have_rows( "storage_feature" ) ) : the_row(); ?>
                                    <?php 
                                        $image = get_sub_field( 'image' );
                                        $content = get_sub_field( 'content' );
                                    ?>
                                    <div>
                                        <?php echo wp_get_attachment_image($image["ID"], "full"); ?>
                                        <div><?php echo $content; ?></div>
                                    </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- end Services -->

				<!-- Reservation CTA -->
				<?php get_template_part("/templates/template-parts/reservation-cta"); ?>
				<!-- end Reservation CTA -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>