function header_initialize(){
	var headerHeight = jQuery('#masthead').outerHeight();
	if(!jQuery('.home')[0]){
		jQuery('.site-top-banner').css('paddingTop', headerHeight);
	}else{
		jQuery('.site-top-banner .overlay').css('paddingTop', headerHeight / 2);
	}
	jQuery('#masthead').fadeIn();

	if(jQuery(window).width() < 769){
		jQuery('nav.navbar').css('max-height', 'calc( 100vh - '+ headerHeight +'px )');
	}else{
		jQuery('nav.navbar').css('max-height', 'auto');
	}
}
