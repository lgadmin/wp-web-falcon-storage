// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapser').fadeToggle();
      	});

        $('.spring-special-mobile .fas').click(function(){
            $('.spring-special-mobile').toggleClass('spring-special-mobile-toggle');
            $(this).toggleClass('spring-special-arrow-rotate');
        })

        // toggle submenu
        $("#menu-main-menu > li > .dropdown-toggle").click(function(){
            $(this).next(".dropdown-menu").toggleClass("show-dropdown");
        })

      //If Mega Menu
      /*$('.mobile-toggle').on('click', function(){
        $('.mega-menu-toggle').click();
      });*/

      
      $('.home-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              dots: true
            }
          }
        ]
      });
  
        
    });

}(jQuery));