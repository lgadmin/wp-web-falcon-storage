// Window Scroll Handler

(function($) {

    $(window).on('scroll', function(){
        var threshold = 200; // number of pixels before bottom of page that you want to start fading
        var op = (($(document).height() - $(window).height()) - $(window).scrollTop()) / threshold;
        if( op <= 0 ){
            $(".spring-special").hide();
        } else {
            $(".spring-special").show();
        }
        $(".spring-special").css("opacity", op ); 

    })

}(jQuery));